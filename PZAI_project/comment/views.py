from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination

from .models import Comment
from .serializers import CommentSerializer, CommentDetailSerializer


class CommentCreateView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CommentSerializer

class CommentQueryView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CommentDetailSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        return Comment.objects.filter(related_company=self.kwargs['company_id']).order_by('-rating_up', 'rating_down')

@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticatedOrReadOnly])
def like(request, company_id, comment_id):
    comment = Comment.objects.get(pk=comment_id)
    rating_up = comment.rating_up + 1
    comment_update_data = {"rating_up": rating_up}
    comment_serializer = CommentSerializer(comment, data=comment_update_data, partial=True)
    message = {}
    if comment_serializer.is_valid():
        comment_serializer.save()
        message["success"] = "Update Successful"
        return Response(data=message)
    return Response(comment_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticatedOrReadOnly])
def dislike(request, company_id, comment_id):
    comment = Comment.objects.get(pk=comment_id)
    rating_down = comment.rating_down + 1
    comment_update_data = {"rating_down": rating_down}
    comment_serializer = CommentSerializer(comment, data=comment_update_data, partial=True)
    message = {}
    if comment_serializer.is_valid():
        comment_serializer.save()
        message["success"] = "Update Successful"
        return Response(data=message)
    return Response(comment_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
