from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

from company.models import Company


class Comment(models.Model):
    comment = models.CharField(max_length=255)
    owner_user_account = models.ForeignKey(User, on_delete=CASCADE, related_name='comment_owner_account')
    related_company = models.ForeignKey(Company, on_delete=CASCADE, related_name='company_account')
    rating_up = models.FloatField(default=0)
    rating_down = models.FloatField(default=0)
