from rest_framework import serializers
from django.contrib.auth.models import User

from company.models import Company

from .models import Comment
from company.serializers import CompanySerializer

class CurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'id')
        read_only_fields = ('id',)

class CommentSerializer(serializers.ModelSerializer):
    # owner_user_account = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    # related_company = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all())
    class Meta:
        model = Comment
        fields = ['id', 'comment', 'owner_user_account', 'related_company', 'rating_up', 'rating_down']
        read_only_fields = ('id', 'owner_user_account', 'related_company',)

    def create(self, validated_data):
        company = Company.objects.get(pk=self.context.get('request').parser_context.get('kwargs').get('company_id'))

        comment = Comment.objects.create(
            comment=validated_data['comment'],
            owner_user_account=self.context['request'].user,
            related_company = company
        )

        comment.save()

        return comment

class CommentDetailSerializer(CommentSerializer):
    owner_user_account = CurrentUserSerializer(read_only=True)
    related_company = CompanySerializer(read_only=True)