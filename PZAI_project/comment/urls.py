from django.urls import path

from .views import CommentCreateView, CommentQueryView, like, dislike

app_name = "comment"


urlpatterns = (
    path('company/<int:company_id>/comment', CommentCreateView.as_view(), name='create_company'),
    path('company/<int:company_id>/comment/search', CommentQueryView.as_view(), name='query_company'),
    path('company/<int:company_id>/comment/<int:comment_id>/like', like, name='like'),
    path('company/<int:company_id>/comment/<int:comment_id>/dislike', dislike, name='like')
)