from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'city', 'addressLine', 'postCode', 'taxId', 'regon', 'rating_sum', 'rating_count']
        read_only_fields = ('id',)

    def create(self, validated_data):
        company_for_user = Company.objects.filter(owner_user_account=self.context['request'].user)

        if company_for_user and not self.context['request'].user.is_staff:
            raise ValidationError("User can only have one company")

        company = Company.objects.create(
            name=validated_data['name'],
            owner_user_account=self.context['request'].user,
            city=validated_data['city'],
            addressLine=validated_data['addressLine'],
            postCode=validated_data['postCode'],
            longitude=0,
            latitude=0,
            taxId=validated_data['taxId'],
            regon=validated_data['regon'],
        )

        company.save()

        return company

class VoteSerializer(serializers.Serializer):
    rating = serializers.FloatField(max_value=5, min_value=0)
