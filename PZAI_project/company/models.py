from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

class Vote(object):
    def __init__(self, rating):
        self.rating = rating

class Company(models.Model):
    name = models.CharField(max_length=50)
    owner_user_account = models.ForeignKey(User, on_delete=CASCADE, related_name='owner_account')
    longitude = models.FloatField()
    latitude = models.FloatField()
    verified = models.BooleanField(default=False)
    taxId = models.CharField(max_length=10, null=True)
    regon = models.CharField(max_length=14, null=True)
    city = models.CharField(max_length=50)
    addressLine = models.CharField(max_length=255)
    postCode = models.CharField(max_length=15)
    isVerified = models.BooleanField(default=False)
    rating_sum = models.FloatField(default=0)
    rating_count = models.FloatField(default=0)
