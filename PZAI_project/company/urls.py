from django.urls import path

from company.views import CompanyCreateView, CompanyQueryView, CompanyGetView, vote

app_name = "company"


urlpatterns = (
    path('company', CompanyCreateView.as_view(), name='create_company'),
    path('company/search', CompanyQueryView.as_view(), name='query_company'),
    path('company/<pk>', CompanyGetView.as_view(), name='get_company'),
    path('company/<int:id>/vote', vote, name='vote')
)
