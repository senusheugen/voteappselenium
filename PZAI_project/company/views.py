from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from .models import Company
from .serializers import CompanySerializer, VoteSerializer


class CompanyCreateView(generics.CreateAPIView):
    queryset = Company.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CompanySerializer


class CompanyQueryView(generics.ListAPIView):
    queryset = Company.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = CompanySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'city', 'addressLine']


class CompanyGetView(generics.RetrieveAPIView):
    queryset = Company.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = CompanySerializer


@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticatedOrReadOnly])
def vote(request, id):
    company = Company.objects.get(pk=id)
    serializer = VoteSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    vote_object = serializer.validated_data
    rating_sum = company.rating_sum + vote_object['rating']
    rating_count = company.rating_count + 1
    company_update_data = {"rating_sum": rating_sum, "rating_count": rating_count}
    company_serializer = CompanySerializer(company, data=company_update_data, partial=True)
    message = {}
    if company_serializer.is_valid():
        company_serializer.save()
        message["success"] = "Update Successful"
        return Response(data=message)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
