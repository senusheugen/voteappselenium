import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By

from environment_variables import PZAI_HOST, SELENIUM_REMOTE_HOST


class MainPageNavBarTest(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--web-security=no')
        options.add_argument('--ssl-protocol=any')
        options.add_argument('--ignore-certificate-errors=yes')
        options.add_argument('--disable-popup-blocking')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-web-security")
        self.driver = webdriver.Remote(
            command_executor=SELENIUM_REMOTE_HOST,
            options=options
        )

    def test_find_all_navbar_button(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        find_buttons = driver.find_elements(By.CLASS_NAME, "nav-item")
        self.assertEqual(len(find_buttons), 4)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
