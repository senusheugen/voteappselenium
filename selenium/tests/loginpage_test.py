import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from environment_variables import PZAI_HOST, SELENIUM_REMOTE_HOST


class LoginPage(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--web-security=no')
        options.add_argument('--ssl-protocol=any')
        options.add_argument('--ignore-certificate-errors=yes')
        options.add_argument('--disable-popup-blocking')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-web-security")
        self.driver = webdriver.Remote(
            command_executor=SELENIUM_REMOTE_HOST,
            options=options
        )

    def check_navbar_element_menu(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        navbar = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "hamburger-icon")))
        if navbar:
            navbar.click()
        find_login_button = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/login']")))
        find_login_button.click()
        return driver

    def test_fields(self):
        driver = self.check_navbar_element_menu()
        log_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "loginInput")))
        pass_butt = driver.find_element(By.ID, "passwordInput")
        push_log_but = driver.find_element(By.XPATH, "//button[text()='ZALOGUJ']")
        button_list = [log_butt, pass_butt, push_log_but]
        self.assertEqual(len(button_list), 3)

    def test_login_without_data(self):
        driver = self.check_navbar_element_menu()
        WebDriverWait(driver,5).until(EC.element_to_be_clickable((By.XPATH, "//button[text()='ZALOGUJ']"))).click()
        warning_button = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//button[@class='Vue-Toastification__close-button']")))
        self.assertIsNotNone(warning_button)

    def test_login_only_with_login_data(self):
        driver = self.check_navbar_element_menu()
        log_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "loginInput")))
        log_butt.send_keys("testlogin")
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//button[text()='ZALOGUJ']"))).click()
        warning_button = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, "//button[@class='Vue-Toastification__close-button']")))
        self.assertIsNotNone(warning_button)

    def test_login_only_with_password_data(self):
        driver = self.check_navbar_element_menu()
        pass_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "passwordInput")))
        pass_butt.send_keys("testlogin")
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//button[text()='ZALOGUJ']"))).click()
        warning_button = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//button[@class='Vue-Toastification__close-button']")))
        self.assertIsNotNone(warning_button)

    def test_login_with_valid_data(self):
        driver = self.check_navbar_element_menu()
        log_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "loginInput")))
        log_butt.send_keys("testlogin")
        pass_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "passwordInput")))
        pass_butt.send_keys("testlogin")
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//button[text()='ZALOGUJ']"))).click()
        find_unlog_button = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//span[text()='Wyloguj się']")))
        self.assertIsNotNone(find_unlog_button)

    def test_login_with_invalid_data(self):
        driver = self.check_navbar_element_menu()
        log_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "loginInput")))
        log_butt.send_keys("testloginn")
        pass_butt = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "passwordInput")))
        pass_butt.send_keys("testloginn")
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//button[text()='ZALOGUJ']"))).click()
        warning_button = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//button[@class='Vue-Toastification__close-button']")))
        self.assertIsNotNone(warning_button)

    def tearDown(self):
        self.driver.quit()
