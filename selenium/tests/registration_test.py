import time
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

from faker import Faker

from environment_variables import PZAI_HOST, SELENIUM_REMOTE_HOST


class RegistrationPage(unittest.TestCase):

    fake = Faker()
    email_fields_dict = {
        'email': fake.ascii_free_email(),
        'name': fake.first_name(),
        'password': fake.first_name()
    }
    company_fields_dict = {
        'company_name': fake.company(),
        'address': fake.street_name(),
        'postcode': fake.postcode(),
        'city': fake.city(),
        'NIP': fake.country_code(),
        'Region': fake.country()
    }

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--web-security=no')
        options.add_argument('--ssl-protocol=any')
        options.add_argument('--ignore-certificate-errors=yes')
        options.add_argument('--disable-popup-blocking')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-web-security")
        self.driver = webdriver.Remote(
            command_executor=SELENIUM_REMOTE_HOST,
            options=options
        )

    def check_navbar_element_menu(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        navbar = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "hamburger-icon")))
        if navbar:
            navbar.click()
        find_registration_button = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/register']")))
        find_registration_button.click()
        return driver

    def test_check_all_fields(self):
        driver = self.check_navbar_element_menu()
        email_butt = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "emailInput")))
        login_butt = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "loginInput")))
        pass_butt = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "passwordInput")))
        comp_butt = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "companyInput")))
        self.assertIsNotNone(email_butt)
        self.assertIsNotNone(login_butt)
        self.assertIsNotNone(pass_butt)
        self.assertIsNotNone(comp_butt)

    def test_check_req_fields_without_any_data(self):
        driver = self.check_navbar_element_menu()
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_register_by_filling_all_fields(self):
        driver = self.check_navbar_element_menu()
        driver.implicitly_wait(1)
        find_email_field = driver.find_element(By.ID, "emailInput")
        find_login_field = driver.find_element(By.ID, "loginInput")
        find_password_field = driver.find_element(By.ID, "passwordInput")
        find_email_field.send_keys(self.fake.ascii_free_email())
        find_login_field.send_keys(self.fake.first_name())
        find_password_field.send_keys(self.fake.first_name())
        driver.find_element(By.XPATH, '//button[@class="btn-base btn-main-color w-100"]').click()
        find_unlog_button = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//span[text()='Wyloguj się']")))
        self.assertIsNotNone(find_unlog_button)


    # def test_already_register_user(self):
    #     items = (
    #         {'case': 'Register User should successfully register', 'email': self.email_fields_dict['email'], 'name': self.email_fields_dict['name'], 'password': self.email_fields_dict['password'], 'result': True},
    #         {'case': 'Register Already created User should failed', 'email': self.email_fields_dict['email'], 'name': self.email_fields_dict['name'], 'password': self.email_fields_dict['password'], 'result': False}
    #     )
    #
    #     for item in items:
    #         with self.subTest(item['case']):
    #             driver = self.check_navbar_element_menu()
    #             driver.implicitly_wait(1)
    #             find_email_field = driver.find_element(By.ID, "emailInput")
    #             find_login_field = driver.find_element(By.ID, "loginInput")
    #             find_password_field = driver.find_element(By.ID, "passwordInput")
    #             find_email_field.send_keys(item['email'])
    #             find_login_field.send_keys(item['name'])
    #             find_password_field.send_keys(item['password'])
    #             driver.find_element(By.XPATH, '//button[@class="btn-base btn-main-color w-100"]').click()
    #             driver.implicitly_wait(1)
    #             find_alert_div = driver.find_elements(By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]')
    #             self.assertEqual(len(find_alert_div) == 0, item['result'])


    def test_email_validation_without_at(self):
        driver = self.check_navbar_element_menu()
        find_email_field = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "emailInput")))
        find_email_field.send_keys("testAtgmail.com")
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_email_validation_without_dot(self):
        driver = self.check_navbar_element_menu()
        find_email_field = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "emailInput")))
        find_email_field.send_keys("test@gmailcom")
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_email_validation_without_dot_com(self):
        driver = self.check_navbar_element_menu()
        find_email_field = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "emailInput")))
        find_email_field.send_keys("test@gmail")
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_email_validation_without_name(self):
        driver = self.check_navbar_element_menu()
        find_email_field = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "emailInput")))
        find_email_field.send_keys("@gmail")
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_password_too_short(self):
        driver = self.check_navbar_element_menu()
        driver.implicitly_wait(1)
        find_email_field = driver.find_element(By.ID, "emailInput")
        find_login_field = driver.find_element(By.ID, "loginInput")
        find_password_field = driver.find_element(By.ID, "passwordInput")
        find_email_field.send_keys(self.fake.ascii_free_email())
        find_login_field.send_keys(self.fake.first_name())
        find_password_field.send_keys('test1')
        driver.find_element(By.XPATH, '//button[@class="btn-base btn-main-color w-100"]').click()
        find_unlog_button = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//span[text()='Wyloguj się']")))
        self.assertIsNone(find_unlog_button)

    def test_password_consist_only_number(self):
        driver = self.check_navbar_element_menu()
        driver.implicitly_wait(1)
        find_email_field = driver.find_element(By.ID, "emailInput")
        find_login_field = driver.find_element(By.ID, "loginInput")
        find_password_field = driver.find_element(By.ID, "passwordInput")
        find_email_field.send_keys(self.fake.ascii_free_email())
        find_login_field.send_keys(self.fake.first_name())
        find_password_field.send_keys('123123123123')
        driver.find_element(By.XPATH, '//button[@class="btn-base btn-main-color w-100"]').click()
        find_unlog_button = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//span[text()='Wyloguj się']")))
        self.assertIsNotNone(find_unlog_button)

    def test_company_button_check(self):
        driver = self.check_navbar_element_menu()
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID,"companyInput"))).click()
        find_company_inputbox = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "companyNameInput")))
        self.assertIsNotNone(find_company_inputbox)

    def test_pass_all_registration_fields(self):
        driver = self.check_navbar_element_menu()
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "companyInput"))).click()
        driver.find_element(By.XPATH, '//button[@class="btn-base btn-main-color w-100"]').click()
        find_alert_div = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.XPATH, '//div[@class="Vue-Toastification__toast Vue-Toastification__toast--warning bottom-right"]'))
        )
        self.assertIsNotNone(find_alert_div)

    def test_fill_all_registration_fields(self):
        driver = self.check_navbar_element_menu()
        driver.implicitly_wait(1)
        company_datas = self.company_fields_dict
        find_email_field = driver.find_element(By.ID, "emailInput")
        find_login_field = driver.find_element(By.ID, "loginInput")
        find_password_field = driver.find_element(By.ID, "passwordInput")
        find_email_field.send_keys(self.fake.ascii_free_email())
        find_login_field.send_keys(self.fake.first_name())
        find_password_field.send_keys(self.fake.first_name())
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "companyInput"))).click()
        driver.implicitly_wait(1)
        find_company_name_field = driver.find_element(By.ID, "companyNameInput")
        find_address_field = driver.find_element(By.ID, "companyAddressInput")
        find_postcode_field = driver.find_element(By.ID, "postCodeInput")
        find_city_field = driver.find_element(By.ID, "cityInput")
        find_country_field = driver.find_element(By.ID, "regonInput")
        find_nip_code_field = driver.find_element(By.ID, "nipInput")
        find_company_name_field.send_keys(company_datas['company_name'])
        find_address_field.send_keys(company_datas['address'])
        find_postcode_field.send_keys(company_datas['postcode'])
        find_city_field.send_keys(company_datas['city'])
        find_country_field.send_keys(company_datas['Region'])
        find_nip_code_field.send_keys(company_datas['NIP'])
        driver.execute_script('document.getElementsByClassName("btn-base btn-main-color w-100")[0].scrollIntoView();')
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="btn-base btn-main-color w-100"]'))).click()
        find_unlog_button = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//span[text()='Wyloguj się']")))
        self.assertIsNotNone(find_unlog_button)

    def tearDown(self):
        self.driver.quit()
