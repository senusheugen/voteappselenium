import unittest
import random

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from environment_variables import PZAI_HOST, SELENIUM_REMOTE_HOST


class LoginPage(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--web-security=no')
        options.add_argument('--ssl-protocol=any')
        options.add_argument('--ignore-certificate-errors=yes')
        options.add_argument('--disable-popup-blocking')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-web-security")
        self.driver = webdriver.Remote(
            command_executor=SELENIUM_REMOTE_HOST,
            options=options
        )

    def check_navbar_element_menu(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        navbar = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "hamburger-icon")))
        if navbar:
            navbar.click()
        return driver


    def login_user_and_go_to_opinie_page(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        driver.implicitly_wait(3)
        navbar = driver.find_elements(By.ID, "hamburger-icon")
        if len(navbar) == 1:
            navbar[0].click()
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/login']"))).click()
        driver.implicitly_wait(1)
        log_butt = driver.find_element(By.ID, "loginInput")
        log_butt.send_keys("testlogin")
        log_butt = driver.find_element(By.ID, "passwordInput")
        log_butt.send_keys("testlogin")
        driver.implicitly_wait(1)
        driver.find_element(By.XPATH, "//button[text()='ZALOGUJ']").click()
        driver.implicitly_wait(1)
        driver.find_element(By.XPATH, "//a[@href='/opinie']").click()
        return driver

    def test_check_all_fields_as_unregister_user(self):
        driver = self.check_navbar_element_menu()
        find_div_with_company = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//div[@class='row pt-2 ps-2 pe-2']")))
        self.assertIsNotNone(find_div_with_company)

    def test_check_all_company(self):
        driver = self.check_navbar_element_menu()
        find_companies = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3 pb-3 custom-container']"))
        )
        random_company = random.choice(find_companies)
        random_company.click()
        find_unlog_text = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//div[text()='Zaloguj się aby dodać swoją opinię.']")))
        self.assertIsNotNone(find_unlog_text)

    def test_check_all_company_as_register_user(self):
        driver = self.login_user_and_go_to_opinie_page()
        find_div_with_company = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//div[@class='row pt-2 ps-2 pe-2']")))
        self.assertIsNotNone(find_div_with_company)

    def test_check_random_company_recensia_fields_as_register_user(self):
        driver = self.login_user_and_go_to_opinie_page()
        find_companies = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3 pb-3 custom-container']"))
        )
        random_company = random.choice(find_companies)
        random_company.click()
        driver.implicitly_wait(3)
        find_stars = driver.find_element(By.XPATH, "//div[@class='container-custom-stars']")
        find_text_field = driver.find_element(By.XPATH, "//div[@class='form-floating p-0 m-0']")
        self.assertIsNotNone(find_stars)
        self.assertIsNotNone(find_text_field)

    # def test_put_random_star_to_company(self):
    #     driver = self.login_user_and_go_to_opinie_page()
    #     find_companies = WebDriverWait(driver, 3).until(
    #         EC.presence_of_element_located((By.XPATH, "//div[@class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3 pb-3 custom-container']"))
    #     )
    #     random.choice(find_companies).click()
    #     time.sleep(1)
    #     find_all_star = driver.find_element(By.XPATH, "//div[@class='container-custom-stars']")
    #     star_butt = driver.find_elements(By.XPATH, "//fieldset[@class = 'rating']")
    #     # va = random.choice(star_butt)
    #     # va.click()
    #     # find_all_star.click()
    #     star_butt.click()
    #     time.sleep(3)
    #     driver.find_element(By.XPATH, "//button[text()= 'Prześlij ocenę']").click()
    #     """
    #     DOESN'T WORK
    #     """

    def test_double_star_entering_as_one_user(self):
        driver = self.login_user_and_go_to_opinie_page()
        find_companies = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3 pb-3 custom-container']"))
        )
        random.choice(find_companies).click()
        driver.implicitly_wait(1)
        driver.find_element(By.XPATH, "//button[text()= 'Prześlij ocenę']").click()
        driver.find_element(By.XPATH, "//button[text()= 'Prześlij ocenę']").click()
        #TODO

    def tearDown(self):
        self.driver.quit()
