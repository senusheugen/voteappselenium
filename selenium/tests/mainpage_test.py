import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from environment_variables import PZAI_HOST, SELENIUM_REMOTE_HOST


class MainPageNavBarTest(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--web-security=no')
        options.add_argument('--ssl-protocol=any')
        options.add_argument('--ignore-certificate-errors=yes')
        options.add_argument('--disable-popup-blocking')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-web-security")
        self.driver = webdriver.Remote(
            command_executor=SELENIUM_REMOTE_HOST,
            options=options
        )

    def check_navbar_element_menu(self):
        driver = self.driver
        driver.get(PZAI_HOST)
        navbar = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "hamburger-icon")))
        if navbar:
            navbar.click()
        return driver

    def test_already_register_user(self):
        items = (
            {'case': 'Test registration button presence', 'element': (By.XPATH, "//a[@href='/register']")},
            {'case': 'Test login button presence', 'element': (By.XPATH, "//a[@href='/login']")},
            {'case': 'Test main page button presence', 'element': (By.XPATH, "//a[@href='/']")},
            {'case': 'Test opinie button presence', 'element': (By.XPATH, "//a[@href='/opinie']")}
        )

        for item in items:
            with self.subTest(item['case']):
                driver = self.check_navbar_element_menu()
                find_login_button = WebDriverWait(driver, 5).until(
                    EC.presence_of_element_located(item['element']))
                self.assertIsNotNone(find_login_button)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
