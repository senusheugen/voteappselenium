import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import OpinionsMainView from '../views/OpinionsMainView.vue'
import RegisterView from '../views/RegisterView.vue'
import LoginView from '../views/LoginView.vue'
import AdminRegisterCompanyView from "@/views/AdminRegisterCompanyView.vue";
import CompanyView from "@/views/CompanyView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'BYŁEM TU',
      component: HomeView
    },
    {
      path: '/opinie',
      name: 'BYŁEM TU - Opinie',
      component: OpinionsMainView
    },
    {
      path: '/register',
      name: 'BYŁEM TU - Rejestracja',
      component: RegisterView
    },
    {
      path: '/login',
      name: 'BYŁEM TU - Logowanie',
      component: LoginView
    },
    {
      path: '/add-company',
      name: 'BYŁEM TU - Dodaj firmę',
      component: AdminRegisterCompanyView
    },
    {
      path: '/firma/:id',
      name: 'firma',
      component: CompanyView
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (typeof to.name === "string") {
    document.title = to.name;
  }
  next();
})

export default router
