import {LocalStorageHelper} from "@/helpers/localStorageHelper";
import type {PostCommentRequest} from "@/api/types/postCommentRequest";
import {ApiConsts} from "@/api/consts";
import type {GetCommentsPagedResponse} from "@/api/types/getCommentsPagedResponse";

export default class CommentService {
    public static serviceName: string = "comment"

    localStorageHelper: LocalStorageHelper;

    constructor() {
        this.localStorageHelper = new LocalStorageHelper();
    }

    public async create(postCommentRequest: PostCommentRequest, companyId: number): Promise<Boolean> {
        const tokens = this.localStorageHelper.getTokens();
        const accessToken = tokens.access;

        const request = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify(postCommentRequest)
        }

        const response = await fetch(`${ApiConsts.apiAddress}/company/${companyId}/comment`, request);

        if (response.status == 201) {
            return true;
        }

        console.error(response.status)
        console.error(await response.json())
        return false;
    }

    public async fetchComments(companyId: number, page: number = 1): Promise<GetCommentsPagedResponse | null> {
        const response = await fetch(`${ApiConsts.apiAddress}/company/${companyId}/comment/search?page=${page}`);

        if (response.status == 200) {
            return await response.json()
        }

        return null;
    }

    public async rateComment(companyId: number, commentId: number, isLike: boolean = true): Promise<boolean> {
        const tokens = this.localStorageHelper.getTokens();
        const accessToken = tokens.access;

        const request = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            }
        }

        const operation = isLike ? "like" : "dislike"

        const response = await fetch(`${ApiConsts.apiAddress}/company/${companyId}/comment/${commentId}/${operation}`, request);

        if (response.status == 200) {
            return true;
        }

        console.error(response.status)
        console.error(await response.json())
        return false;
    }

    public async fetchCommentsPageFromLink(link: string): Promise<GetCommentsPagedResponse | null> {
        const response = await fetch(link);

        if (response.status == 200) {
            return await response.json()
        }

        return null;
    }
}
