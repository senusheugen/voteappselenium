import type {CompanyRequest} from "@/api/types/companyRequest";
import {LocalStorageHelper} from "@/helpers/localStorageHelper";
import {ApiConsts} from "@/api/consts";
import type {CompanyResponse} from "@/api/types/companyResponse";
import type {CompanyRatingRequest} from "@/api/types/companyRatingRequest";

export default class Company {
    public static serviceName: string = "company"

    localStorageHelper: LocalStorageHelper;

    constructor() {
        this.localStorageHelper = new LocalStorageHelper();
    }

    public async create(companyData: CompanyRequest): Promise<boolean> {
        const tokens = this.localStorageHelper.getTokens();
        const accessToken = tokens.access;

        const request = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify(companyData)
        }

        const response = await fetch(`${ApiConsts.apiAddress}/company`, request);

        if (response.status == 201) {
            return true;
        }

        console.error(await response.json())
        return false;
    }

    public async search(searchString: string): Promise<Array<CompanyResponse> | null> {
        // @ts-ignore
        searchString = searchString.replaceAll(',', '').replaceAll(' ', '%20');

        const response = await fetch(`${ApiConsts.apiAddress}/company/search?search=${searchString}`)

        if (response.status == 200) {
            return await response.json();
        }

        return null;
    }

    public async getById(id: number): Promise<CompanyResponse | null> {
        const response = await fetch(`${ApiConsts.apiAddress}/company/${id}`)

        if (response.status == 200) {
            return await response.json();
        }

        return null;
    }

    public async vote(companyRatingRequest: CompanyRatingRequest, id: number): Promise<Boolean> {
        const tokens = this.localStorageHelper.getTokens();
        const accessToken = tokens.access;

        const request = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify(companyRatingRequest)
        }

        const response = await fetch(`${ApiConsts.apiAddress}/company/${id}/vote`, request);

        if (response.status == 200) {
            return true;
        }

        console.error(response.status)
        console.error(await response.json())
        return false;
    }
}
