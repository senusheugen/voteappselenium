export type TokenResponse = {
    refresh: string,
    access: string
}

export type RefreshTokenResponse = {
    access: string
}
