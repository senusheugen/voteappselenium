export type CompanyRequest = {
    name: string,
    city: string,
    addressLine: string,
    postCode: string,
    taxId: string | null,
    regon: string | null
}
