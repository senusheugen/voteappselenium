import type {GetCommentResponse} from "@/api/types/getCommentResponse";

export type GetCommentsPagedResponse = {
    count: number,
    next: string,
    previous: string,
    results: Array<GetCommentResponse>
}
