export type Token = {
    exp: number,
    iat: number,
    jti: string,
    token_type: string,
    user_id: number,
    username: string
    isStaff: boolean
}
