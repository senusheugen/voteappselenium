import type {UserResponse} from "@/api/types/userResponse";
import type {CompanyResponse} from "@/api/types/companyResponse";

export type GetCommentResponse = {
    id: number,
    comment: string,
    owner_user_account: UserResponse,
    related_company: CompanyResponse,
    rating_up: number,
    rating_down: number
}
