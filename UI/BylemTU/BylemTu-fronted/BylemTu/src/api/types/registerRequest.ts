export type RegisterRequest = {
    username: string,
    password: string,
    email: string
}
