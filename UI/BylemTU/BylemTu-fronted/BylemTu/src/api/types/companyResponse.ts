export type CompanyResponse = {
    id: number,
    name: string,
    city: string,
    addressLine: string,
    postCode: string,
    taxId: string,
    regon: string,
    rating_sum: number,
    rating_count: number
}
