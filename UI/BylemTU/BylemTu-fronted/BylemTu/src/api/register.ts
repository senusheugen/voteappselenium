import type {RegisterRequest} from "@/api/types/registerRequest";
import {ApiConsts} from "@/api/consts";

export default class Register {
    public static serviceName: string = "register";

    public async register(registerRequest: RegisterRequest): Promise<boolean> {
        const request = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(registerRequest)
        }

        const response = await fetch(`${ApiConsts.apiAddress}/register`, request)

        if (response.status == 201) {
            return true;
        }

        return false;
    }
}
