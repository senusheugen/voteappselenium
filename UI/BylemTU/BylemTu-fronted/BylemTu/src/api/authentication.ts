import type {LoginRequest} from "@/api/types/loginRequest";
import {ApiConsts} from "@/api/consts";
import type {RefreshTokenResponse, TokenResponse} from "@/api/types/tokenResponse";
import {LocalStorageHelper} from "@/helpers/localStorageHelper";
import {ref} from "vue";
import jwt_decode from "jwt-decode";
import type {Token} from "@/api/types/token";

export default class Authentication {
  public static serviceName: string = "authentication";

  public isLoggedIn = ref(false);
  public isStaff = ref(false);

  private localStorageHelper: LocalStorageHelper;

  constructor() {
    this.localStorageHelper = new LocalStorageHelper();

    this.checkTokenStatus();
  }

  public async login(loginData: LoginRequest): Promise<boolean> {
    const request = {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(loginData)
    }

    const response = await fetch(`${ApiConsts.apiAddress}/api/token`, request);

    if (response.status == 200) {
      this.isLoggedIn.value = true;
      const data: TokenResponse = await response.json();
      this.localStorageHelper.setTokens(data);
      this.setIsStaff(data.access)
      return true;
    }

    return false;
  }

  public logOut(): void {
    this.localStorageHelper.deleteTokens();
    this.isLoggedIn.value = false;
    this.isStaff.value = false;
  }

  private static async refreshToken(token: string): Promise<string> {
    const request = {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({refresh: token})
    }

    const response = await fetch(`${ApiConsts.apiAddress}/api/token/refresh`, request);

    const data: RefreshTokenResponse = await response.json();

    return data.access;
  }

  private checkTokenStatus(): void {
    const tokens = this.localStorageHelper.getTokens();

    if (tokens.access === null || tokens.access === undefined) {
      this.localStorageHelper.deleteTokens()
      this.isLoggedIn.value = false;
      this.isStaff.value = false;
      return;
    }

    const decodedAccess : Token = jwt_decode(tokens.access);
    const decodedRefresh : Token = jwt_decode(tokens.refresh);

    if (Date.now() >= decodedRefresh.exp * 1000) {
      this.localStorageHelper.deleteTokens()
      this.isLoggedIn.value = false;
      this.isStaff.value = false;
      return;
    }

    if (Date.now() >= decodedAccess.exp * 1000) {
      Authentication.refreshToken(tokens.refresh).then((newToken: string)=> {
        this.localStorageHelper.updateAccessToken(newToken);
        this.setIsStaff(newToken)
      });
    }

    const token: string = this.localStorageHelper.getTokens().access;
    this.setIsStaff(token)
    this.isLoggedIn.value = true;
  }

  private setIsStaff(accessToken: string): void {
    const decodedAccess : Token = jwt_decode(accessToken);
    this.isStaff.value = decodedAccess.isStaff;
  }
}

