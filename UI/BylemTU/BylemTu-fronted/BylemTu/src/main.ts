import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import type {PluginOptions} from "vue-toastification";
import Toast, {POSITION} from "vue-toastification";
import "vue-toastification/dist/index.css";
import Authentication from "@/api/authentication.js";
import {LocalStorageHelper} from "@/helpers/localStorageHelper";
import Register from "@/api/register";
import Company from "@/api/company";
import CommentService from "@/api/commentService";

const app = createApp(App)

const options: PluginOptions = {
    shareAppContext: true,
    position: POSITION.BOTTOM_RIGHT
};

app.use(Toast, options);

app.use(router)

app.provide(Authentication.serviceName, new Authentication())
app.provide(LocalStorageHelper.serviceName, new LocalStorageHelper())
app.provide(Register.serviceName, new Register())
app.provide(Company.serviceName, new Company())
app.provide(CommentService.serviceName, new CommentService())

app.mount('#app')
