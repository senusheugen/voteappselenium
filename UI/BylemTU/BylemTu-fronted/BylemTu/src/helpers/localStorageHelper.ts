import type {TokenResponse} from "@/api/types/tokenResponse";

export class LocalStorageHelper {
    public static serviceName: string = "localStorageHelper";

    private lastRouteString: string = 'lastRoute';
    private accessTokenString: string = 'accessToken';
    private refreshTokenString: string = 'refreshToken';

    public setLastRoute(route: string): void {
        localStorage.setItem(this.lastRouteString, route);
    }

    public getLastRoute(): string {
        return localStorage.getItem(this.lastRouteString) ?? '/';
    }

    public setTokens(tokens: TokenResponse): void {
        localStorage.setItem(this.accessTokenString, tokens.access);
        localStorage.setItem(this.refreshTokenString, tokens.refresh);
    }

    public deleteTokens(): void {
        localStorage.removeItem(this.accessTokenString);
        localStorage.removeItem(this.refreshTokenString);
    }

    public updateAccessToken(token: string): void {
        localStorage.setItem(this.accessTokenString, token);
    }

    public getTokens(): TokenResponse {
        return {
            access: localStorage.getItem(this.accessTokenString),
            refresh: localStorage.getItem(this.refreshTokenString),
        } as TokenResponse;
    }
}
